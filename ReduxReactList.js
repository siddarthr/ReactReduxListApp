<html>

<head>
</head>

<body>


	<script src="https://unpkg.com/react@16.2.0/umd/react.development.js"> </script>
	<script src="https://unpkg.com/react-dom@16.2.0/umd/react-dom.development.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/create-react-class@15.6.2/create-react-class.min.js"></script>
	<script src="https://unpkg.com/react-redux@5.0.6/dist/react-redux.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/redux/3.5.2/redux.js" type="text/javascript"></script>
	<script src=" https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.24/browser.min.js"></script>


	<div id="root"></div>
	<script type="text/babel"> 
					
   //component displaying list of book title		
	var BookListComponent  = createReactClass({
			
		displayName: 'BookListComponent',

		render(){
			
			if(!this.props.BookListReducer){
				return (<div>map is empty </div>);
			} 
			
		    var htmlString = this.props.BookListReducer.map(this.renderItem);
			return (<ul>{htmlString}</ul>);
    	},
			 
		renderItem:function(book){
	        return (<li>{book.title}</li>);
	    },
		 
	});	

//do mention which reducer you want to read
function mapStateToProps(rootReducer){
    console.log("mapStateToProps is called");
	return{
		BookListReducer:rootReducer.BookListReducer
	};
}

var BookListComponentConnected = ReactRedux.connect(mapStateToProps)(BookListComponent);	
	 			
function BookListReducer(){
	return [
	{title : 'javascript'},
	{title : 'harry potter'},
	{title : 'the dart tower'},
	{title : 'Got'},
	{title : 'eloquent ruby'}
	]
}
			
const rootReducer = Redux.combineReducers({
			BookListReducer: BookListReducer
		});			
	
var store = Redux.createStore(rootReducer);		
					
ReactDOM.render(<ReactRedux.Provider store={store}><BookListComponentConnected/></ReactRedux.Provider>, document.getElementById('root'));
	

    </script>
</body>

</html>